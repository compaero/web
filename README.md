## han_web

han_web is a base Extra template that is useful when wanting to create a new
Extra for MODx Revolution. One can git archive from this repository to start
with all the file structure for beginning MODx Extra development pre-setup.

## Copyright Information

han_web is distributed as GPL (as MODx Revolution is), but the copyright owner
(Shaun McCormick) grants all users of han_web the ability to modify, distribute
and use han_web in MODx development as they see fit, as long as attribution
is given somewhere in the distributed source of all derivative works.