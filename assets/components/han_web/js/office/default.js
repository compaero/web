Ext.onReady(function() {
	han_web.config.connector_url = OfficeConfig.actionUrl;

	var grid = new han_web.panel.Home();
	grid.render('office-han_web-wrapper');

	var preloader = document.getElementById('office-preloader');
	if (preloader) {
		preloader.parentNode.removeChild(preloader);
	}
});