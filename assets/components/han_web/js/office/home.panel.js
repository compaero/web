han_web.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'han_web-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: false,
			hideMode: 'offsets',
			items: [{
				title: _('han_web_items'),
				layout: 'anchor',
				items: [{
					html: _('han_web_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'han_web-grid-items',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	han_web.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(han_web.panel.Home, MODx.Panel);
Ext.reg('han_web-panel-home', han_web.panel.Home);
