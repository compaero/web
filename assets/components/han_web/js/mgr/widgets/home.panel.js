han_web.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'han_web-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('han_web') + '</h2>',
			cls: '',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
			items: [{
				title: _('han_web_items'),
				layout: 'anchor',
				items: [{
					html: _('han_web_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'han_web-grid-items',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	han_web.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(han_web.panel.Home, MODx.Panel);
Ext.reg('han_web-panel-home', han_web.panel.Home);
