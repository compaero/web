han_web.page.Home = function (config) {
	config = config || {};
	Ext.applyIf(config, {
		components: [{
			xtype: 'han_web-panel-home', renderTo: 'han_web-panel-home-div'
		}]
	});
	han_web.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(han_web.page.Home, MODx.Component);
Ext.reg('han_web-page-home', han_web.page.Home);