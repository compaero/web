var han_web = function (config) {
	config = config || {};
	han_web.superclass.constructor.call(this, config);
};
Ext.extend(han_web, Ext.Component, {
	page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('han_web', han_web);

han_web = new han_web();