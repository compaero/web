<?php
// Boot up MODX
if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php')) {
    require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
} else {
    require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.core.php';
}
require_once MODX_CORE_PATH . 'model/modx/modx.class.php';

$modx = new modX();
$modx->initialize('web');
$modx->getService('error','error.modError', '', '');
$modx->log(1, 'REQUEST: ' . print_r($_REQUEST, 1));
// Boot up any service classes or packages (models) you will need
$path = $modx->getOption('han_web.core_path', null,
        $modx->getOption('core_path').'components/han_web/') . 'model/han_web/';
/** @var evCore $hw */
$hw = $modx->getService('han_web', 'han_web', $path);
// Load the modRestService class and pass it some basic configuration
/** @var modRestService $rest */
$rest = $modx->getService('rest', 'HanWebRestService', $hw->config['corePath'] . 'model/han_web/', array(
    'basePath' => $hw->config['corePath'] . 'rest/controllers/',
    'controllerClassSeparator' => '',
    'controllerClassPrefix' => 'HanWeb',
    'xmlRootNode' => 'response',
));
require_once $hw->config['corePath'] . 'rest/basecontroller.php';
require_once $hw->config['corePath'] . 'rest/baseprocessorcontroller.php';
// Prepare the request
$rest->prepare();
// Make sure the user has the proper permissions, send the user a 401 error if not
if (!$rest->checkPermissions()) {
    $rest->sendUnauthorized(true);
}
// Run the request
$rest->process();