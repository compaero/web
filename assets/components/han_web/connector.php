<?php
/** @noinspection PhpIncludeInspection */
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var han_web $han_web */
$han_web = $modx->getService('han_web', 'han_web', $modx->getOption('han_web_core_path', null, $modx->getOption('core_path') . 'components/han_web/') . 'model/han_web/');
$modx->lexicon->load('han_web:default');

// handle request
$corePath = $modx->getOption('han_web_core_path', null, $modx->getOption('core_path') . 'components/han_web/');
$path = $modx->getOption('processorsPath', $han_web->config, $corePath . 'processors/');
$modx->request->handleRequest(array(
	'processors_path' => $path,
	'location' => '',
));