<?php

/**
 * The base class for han_web.
 */
class han_web {
	/* @var modX $modx */
	public $modx;
    /** @var EmerCoin $emc */
	public $emc;


	/**
	 * @param modX $modx
	 * @param array $config
	 */
	function __construct(modX &$modx, array $config = array()) {
		$this->modx =& $modx;

		$corePath = $this->modx->getOption('han_web_core_path', $config, $this->modx->getOption('core_path') . 'components/han_web/');
		$assetsUrl = $this->modx->getOption('han_web_assets_url', $config, $this->modx->getOption('assets_url') . 'components/han_web/');
		$connectorUrl = $assetsUrl . 'connector.php';

		$this->config = array_merge(array(
			'assetsUrl' => $assetsUrl,
			'cssUrl' => $assetsUrl . 'css/',
			'jsUrl' => $assetsUrl . 'js/',
			'imagesUrl' => $assetsUrl . 'images/',
			'connectorUrl' => $connectorUrl,

			'corePath' => $corePath,
			'modelPath' => $corePath . 'model/',
			'chunksPath' => $corePath . 'elements/chunks/',
			'templatesPath' => $corePath . 'elements/templates/',
			'chunkSuffix' => '.chunk.tpl',
			'snippetsPath' => $corePath . 'elements/snippets/',
			'processorsPath' => $corePath . 'processors/'
		), $config);

		$this->modx->addPackage('han_web', $this->config['modelPath']);
		$this->modx->lexicon->load('han_web:default');
	}


	public function emcGet()
    {
        if (!$this->emc = $this->modx->getService('emercoin', 'EmerCoin', $this->modx->getOption('emercoin_core_path', null, $this->modx->getOption('core_path') . 'components/emercoin/') . 'model/emercoin/', array())) {
            return 'Could not load EmerCoin class!';
        }
        return $this->emc;
    }

    public function emcRead($name)
    {
        if ($this->emcGet()) {
            return $this->emc->query('name_show', array($name));
        }
        return false;
    }

    public function emcWrite($name, $value, $days = 30)
    {
        if ($this->emcGet()) {
            $checkIsset = $this->emcRead($name);
//            $this->modx->log(1, 'check isset: ' . print_r($checkIsset, 1));
            if (empty($checkIsset['error'])) {
                return $this->emc->query('name_update', array($name, $value, $days));
            } else {
                return $this->emc->query('name_new', array($name, $value, $days));
            }
        }
        return false;
    }

}