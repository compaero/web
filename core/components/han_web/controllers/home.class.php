<?php

/**
 * The home manager controller for han_web.
 *
 */
class han_webHomeManagerController extends han_webMainController {
	/* @var han_web $han_web */
	public $han_web;


	/**
	 * @param array $scriptProperties
	 */
	public function process(array $scriptProperties = array()) {
	}


	/**
	 * @return null|string
	 */
	public function getPageTitle() {
		return $this->modx->lexicon('han_web');
	}


	/**
	 * @return void
	 */
	public function loadCustomCssJs() {
		$this->addCss($this->han_web->config['cssUrl'] . 'mgr/main.css');
		$this->addCss($this->han_web->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
		$this->addJavascript($this->han_web->config['jsUrl'] . 'mgr/misc/utils.js');
		$this->addJavascript($this->han_web->config['jsUrl'] . 'mgr/widgets/items.grid.js');
		$this->addJavascript($this->han_web->config['jsUrl'] . 'mgr/widgets/items.windows.js');
		$this->addJavascript($this->han_web->config['jsUrl'] . 'mgr/widgets/home.panel.js');
		$this->addJavascript($this->han_web->config['jsUrl'] . 'mgr/sections/home.js');
		$this->addHtml('<script type="text/javascript">
		Ext.onReady(function() {
			MODx.load({ xtype: "han_web-page-home"});
		});
		</script>');
	}


	/**
	 * @return string
	 */
	public function getTemplateFile() {
		return $this->han_web->config['templatesPath'] . 'home.tpl';
	}
}