<?php

/**
 * Enable an Item
 */
class han_webItemEnableProcessor extends modObjectProcessor {
	public $objectType = 'han_webItem';
	public $classKey = 'han_webItem';
	public $languageTopics = array('han_web');
	//public $permission = 'save';


	/**
	 * @return array|string
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}

		$ids = $this->modx->fromJSON($this->getProperty('ids'));
		if (empty($ids)) {
			return $this->failure($this->modx->lexicon('han_web_item_err_ns'));
		}

		foreach ($ids as $id) {
			/** @var han_webItem $object */
			if (!$object = $this->modx->getObject($this->classKey, $id)) {
				return $this->failure($this->modx->lexicon('han_web_item_err_nf'));
			}

			$object->set('active', true);
			$object->save();
		}

		return $this->success();
	}

}

return 'han_webItemEnableProcessor';
