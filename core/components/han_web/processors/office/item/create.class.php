<?php

/**
 * Create an Item
 */
class han_webOfficeItemCreateProcessor extends modObjectCreateProcessor {
	public $objectType = 'han_webItem';
	public $classKey = 'han_webItem';
	public $languageTopics = array('han_web');
	//public $permission = 'create';


	/**
	 * @return bool
	 */
	public function beforeSet() {
		$name = trim($this->getProperty('name'));
		if (empty($name)) {
			$this->modx->error->addField('name', $this->modx->lexicon('han_web_item_err_name'));
		}
		elseif ($this->modx->getCount($this->classKey, array('name' => $name))) {
			$this->modx->error->addField('name', $this->modx->lexicon('han_web_item_err_ae'));
		}

		return parent::beforeSet();
	}

}

return 'han_webOfficeItemCreateProcessor';