<?php

/**
 * Get an Item
 */
class han_webOfficeItemGetProcessor extends modObjectGetProcessor {
	public $objectType = 'han_webItem';
	public $classKey = 'han_webItem';
	public $languageTopics = array('han_web:default');
	//public $permission = 'view';


	/**
	 * We doing special check of permission
	 * because of our objects is not an instances of modAccessibleObject
	 *
	 * @return mixed
	 */
	public function process() {
		if (!$this->checkPermissions()) {
			return $this->failure($this->modx->lexicon('access_denied'));
		}

		return parent::process();
	}

}

return 'han_webOfficeItemGetProcessor';