<?php

class HanWebEvents extends HanWebBaseRestController {
    public $parent = 3;
    public $template = 3;
    public $addresses = array();

    /** {@inheritdoc} */
    protected function prepareListQueryBeforeCount(xPDOQuery $c) {
        if ($parent = $this->getProperty('type')) {
            $this->parent = $parent;
        }
        $c->where(array('parent' => $this->parent));

        $q = $this->modx->newQuery('modResource');
        $q->where(array('template' => 2));
        $this->modx->log(1, print_r($this->getProperties(),1));
        if ($lat = $this->getProperty('latitude')) {
            $lng = $this->getProperty('longitude');
            $radius = $this->getProperty('accuracy') / 1000;

//            $q->select(array('modResource.*,  (
//            6371 * acos (
//            cos ( radians(' . $lat .') )
//             * cos( radians( modResource.introtext ) )
//             * cos( radians( modResource.description ) - radians(' . $lng .') )
//            + sin ( radians(' . $lat .') )
//             * sin( radians( modResource.introtext ) )
//            )
//            ) AS `distance`'));
            $q->where(array('(
            6371 * acos (
            cos ( radians(' . $lat .') )
             * cos( radians( modResource.introtext ) )
             * cos( radians( modResource.description ) - radians(' . $lng .') )
            + sin ( radians(' . $lat .') )
             * sin( radians( modResource.introtext ) )
            )
            ) < ' . $radius ));
        }
//        $q->sortby('distance');
        $q->prepare();
        $this->modx->log(1, $q->toSQL());
        $q->stmt->execute();
        $addresses = $q->stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($addresses as $a) {
            $addr = array();
            foreach ($a as $k => $v) {
                $addr[substr($k, 12)] = $v;
            }
            $this->addresses[$addr['id']] = $addr;
        }
        $this->modx->log(1, print_r($this->addresses, 1));

        return $c;
    }




    public function getList() {
        $this->getProperties();
        $c = $this->modx->newQuery($this->classKey);
        $c = $this->addSearchQuery($c);
        $c = $this->prepareListQueryBeforeCount($c);
        $total = $this->modx->getCount($this->classKey,$c);
        $alias = !empty($this->classAlias) ? $this->classAlias : $this->classKey;
        $c->select($this->modx->getSelectColumns($this->classKey,$alias));

        $c = $this->prepareListQueryAfterCount($c);

        $c->sortby($this->getProperty($this->getOption('propertySort','sort'),$this->defaultSortField),$this->getProperty($this->getOption('propertySortDir','dir'),$this->defaultSortDirection));
        $limit = $this->getProperty($this->getOption('propertyLimit','limit'),$this->defaultLimit);
        if (empty($limit)) $limit = $this->defaultLimit;
        $c->limit($limit,$this->getProperty($this->getOption('propertyOffset','start'),$this->defaultOffset));
        $objects = $this->modx->getCollection($this->classKey,$c);
        if (empty($objects)) $objects = array();
        $list = array();
        /** @var xPDOObject $object */
        foreach ($objects as $object) {
            $aa = array();
            $addresses = explode(',', $object->get('longtitle'));
            foreach ($addresses as $a) {
                if (isset($this->addresses[$a])) {
                    $aa[] = $this->addresses[$a];
//                    break;
                }
            }
            $object->set('longtitle', $this->modx->toJSON($aa));
            if (($this->getProperty('latitude') and !empty($aa)) or !$this->getProperty('latitude')) {

                $list[] = $this->prepareListObject($object);
            }
        }
        return $this->collection($list,$total);
    }



}