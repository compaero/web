<?php

class HanWebAddresses extends HanWebBaseRestController
{
    public $parent = 2;
    public $template = 2;

    /** {@inheritdoc} */
    public function initialize()
    {
        parent::initialize();
        $this->whereCondition['parent'] = $this->parent;
        $this->whereCondition['template'] = $this->template;
    }
}