<?php

class HanWebCompaigns extends HanWebBaseRestController
{
    public $parent = 28;
    public $template = 5;

    /** {@inheritdoc} */
    public function initialize()
    {
        parent::initialize();
        $this->whereCondition['parent'] = $this->parent;
        $this->whereCondition['template'] = $this->template;
    }



    public function post()
    {
        $content = array(
            'NAME=' . $this->getProperty('name'),
            'EVENT=' . $this->getProperty('event'),
            'COUNT=' . $this->getProperty('count'),
            'START=' . $this->getProperty('start'),
            'END=' . $this->getProperty('end'),
            'COINS=' . $this->getProperty('count') * 0.57,
        );
        $content = implode(PHP_EOL, $content);
        $name = 'adhack:' . $this->modx->user->id . '__' . $this->getProperty('name') . '__' . $this->getProperty('event') . '__' . time();

        $this->setProperty('pagetitle', $name);
//        $this->setProperty('longtitle', );
        $this->setProperty('content', $content);
        $this->setProperty('parent', $this->parent);
        $this->setProperty('published', 1);

        /** @var han_web $han_web */
        if ($han_web = $this->modx->getService('han_web')) {
            $result = $han_web->emcWrite($name, $content);
            $this->setProperty('introtext', $result['result']);
        }


        parent::post();

    }
}