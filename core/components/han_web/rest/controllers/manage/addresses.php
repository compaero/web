<?php

require_once dirname(dirname(__FILE__)) . '/addresses.php';

class HanWebManageAddresses extends HanWebAddresses
{

    /** {@inheritdoc} */
    public function initialize()
    {
        parent::initialize();
        $this->whereCondition['createdby'] = $this->modx->user->id;
    }
}