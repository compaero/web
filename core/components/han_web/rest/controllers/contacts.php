<?php

class HanWebContacts extends HanWebBaseRestController
{
    public $parent = 9;
    public $template = 4;

    /** {@inheritdoc} */
    public function initialize()
    {
        parent::initialize();
        $this->whereCondition['parent'] = $this->parent;
        $this->whereCondition['template'] = $this->template;
    }

    public function post()
    {
        $campaign = $this->getProperty('campaign');
        $name = 'adhack:' . $campaign . '__' . $this->modx->user->id . '__' . time();
        $content = 'CAMPAIGN=' . $campaign . PHP_EOL .
            'USER=' . $this->modx->user->id . PHP_EOL .
            'ADDRESS=' . $this->getProperty('address') . PHP_EOL .
            'TIME=' . time();
        $this->setProperty('pagetitle', $name);
        $this->setProperty('longtitle', $campaign);
        $this->setProperty('content', $content);
        $this->setProperty('parent', $this->parent);
        $this->setProperty('published', 1);

        /** @var han_web $han_web */
        if ($han_web = $this->modx->getService('han_web')) {
            $result = $han_web->emcWrite($name, $content);
            $this->setProperty('introtext', $result['result']);
        }


        parent::post();

    }
}