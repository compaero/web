<?php

class HanWebUsers extends HanWebBaseProcessorRestController
{
    public $processorGet = 'security/user/get';
    public $processorGetList = 'security/user/getlist';
    public $processorPut = 'auth/create';
    public $processorPost = 'auth/create';


    /** {@inheritdoc} */
    public function post() {
//        $this->processorResultElement = 'object';
        $this->processorPath = MODX_CORE_PATH . 'components/office/processors/';
        parent::put();
    }


    /** {@inheritdoc} */
    public function put() {
//        $this->processorResultElement = 'object';
        $this->processorPath = MODX_CORE_PATH . 'components/office/processors/';
        parent::put();
    }
}