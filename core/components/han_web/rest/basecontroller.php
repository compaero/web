<?php

class HanWebBaseRestController extends modRestController {
    public $classKey = 'modResource';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $whereCondition = array();

    /** {@inheritdoc} */
    protected function prepareListQueryBeforeCount(xPDOQuery $c) {
        if (!empty($this->whereCondition)) {
            $c->where($this->whereCondition);
        }
        return $c;
    }

}