<?php

class HanWebBaseProcessorRestController extends modRestController {
    public $processorGet;
    public $processorGetList;
    public $processorPut;
    public $processorPost;
    public $processorPath;
    public $processorResultElement = 'object';

    /** {@inheritdoc} */
//    protected function prepareListQueryBeforeCount(xPDOQuery $c) {
//        if (!empty($this->whereCondition)) {
//            $c->where($this->whereCondition);
//        }
//        return $c;
//    }



    /**
     * Abstract method for routing GET requests with a primary key passed. Must be defined in your derivative
     * controller.
     * @abstract
     * @param $id
     */
    public function read($id) {
        if (empty($id)) {
            $this->failure($this->modx->lexicon('rest.err_field_ns',array(
                'field' => $this->primaryKeyField,
            )));
            return;
        }
        $this->runProcessor($this->processorGet, array('id' => $id));
    }

    public function getList() {
        $this->processorResultElement = 'results';
        $this->runProcessor($this->processorGetList, $this->getProperties());
    }


    /**
     * Method for routing POST requests. Can be overridden; default behavior automates xPDOObject, class-based requests.
     * @abstract
     */
    public function post() {
        $properties = $this->getProperties();

        if (!empty($this->postRequiredFields)) {
            if (!$this->checkRequiredFields($this->postRequiredFields)) {
                $this->failure($this->modx->lexicon('error'));
                return;
            }
        }

        if (!empty($this->postRequiredRelatedObjects)) {
            if (!$this->checkRequiredRelatedObjects($this->postRequiredRelatedObjects)) {
                $this->failure();
                return;
            }
        }

        $beforePost = $this->beforePost();
        if ($beforePost !== true && $beforePost !== null) {
            $this->failure($beforePost === false ? $this->errorMessage : $beforePost);
            return;
        }
        $this->runProcessor($this->processorPost, $properties);
    }



    /**
     * Handles updating of objects
     */
    public function put() {
        $id = $this->getProperty($this->primaryKeyField,false);
        if (empty($id)) {
            $this->failure($this->modx->lexicon('rest.err_field_ns',array(
                'field' => $this->primaryKeyField,
            )));
            return;
        }
        $this->runProcessor($this->processorPut, $this->getProperties());
    }

    public function runProcessor($processor, $data)
    {
        if (empty($processor)) {
            $this->failure('not allowed');
            return;
        }
        $options = array();
        if (!empty($this->path)) {
            $options['processors_path'] = $this->path;
        }
        $response = $this->modx->runProcessor($processor, $data, $options);
        if ($response->response['success']) {
            $this->success('', $response->response[$this->processorResultElement]);
        } else {
            $this->failure($response->response['message'] ?: $response->response['msg']);
        }
    }

}