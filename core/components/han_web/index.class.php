<?php

/**
 * Class han_webMainController
 */
abstract class han_webMainController extends modExtraManagerController {
	/** @var han_web $han_web */
	public $han_web;


	/**
	 * @return void
	 */
	public function initialize() {
		$corePath = $this->modx->getOption('han_web_core_path', null, $this->modx->getOption('core_path') . 'components/han_web/');
		require_once $corePath . 'model/han_web/han_web.class.php';

		$this->han_web = new han_web($this->modx);
		//$this->addCss($this->han_web->config['cssUrl'] . 'mgr/main.css');
		$this->addJavascript($this->han_web->config['jsUrl'] . 'mgr/han_web.js');
		$this->addHtml('
		<script type="text/javascript">
			han_web.config = ' . $this->modx->toJSON($this->han_web->config) . ';
			han_web.config.connector_url = "' . $this->han_web->config['connectorUrl'] . '";
		</script>
		');

		parent::initialize();
	}


	/**
	 * @return array
	 */
	public function getLanguageTopics() {
		return array('han_web:default');
	}


	/**
	 * @return bool
	 */
	public function checkPermissions() {
		return true;
	}
}


/**
 * Class IndexManagerController
 */
class IndexManagerController extends han_webMainController {

	/**
	 * @return string
	 */
	public static function getDefaultController() {
		return 'home';
	}
}