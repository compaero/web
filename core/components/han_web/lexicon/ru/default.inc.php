<?php
include_once 'setting.inc.php';

$_lang['han_web'] = 'han_web';
$_lang['han_web_menu_desc'] = 'Пример расширения для разработки.';
$_lang['han_web_intro_msg'] = 'Вы можете выделять сразу несколько предметов при помощи Shift или Ctrl.';

$_lang['han_web_items'] = 'Предметы';
$_lang['han_web_item_id'] = 'Id';
$_lang['han_web_item_name'] = 'Название';
$_lang['han_web_item_description'] = 'Описание';
$_lang['han_web_item_active'] = 'Активно';

$_lang['han_web_item_create'] = 'Создать предмет';
$_lang['han_web_item_update'] = 'Изменить Предмет';
$_lang['han_web_item_enable'] = 'Включить Предмет';
$_lang['han_web_items_enable'] = 'Включить Предметы';
$_lang['han_web_item_disable'] = 'Отключить Предмет';
$_lang['han_web_items_disable'] = 'Отключить Предметы';
$_lang['han_web_item_remove'] = 'Удалить Предмет';
$_lang['han_web_items_remove'] = 'Удалить Предметы';
$_lang['han_web_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
$_lang['han_web_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
$_lang['han_web_item_active'] = 'Включено';

$_lang['han_web_item_err_name'] = 'Вы должны указать имя Предмета.';
$_lang['han_web_item_err_ae'] = 'Предмет с таким именем уже существует.';
$_lang['han_web_item_err_nf'] = 'Предмет не найден.';
$_lang['han_web_item_err_ns'] = 'Предмет не указан.';
$_lang['han_web_item_err_remove'] = 'Ошибка при удалении Предмета.';
$_lang['han_web_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['han_web_grid_search'] = 'Поиск';
$_lang['han_web_grid_actions'] = 'Действия';